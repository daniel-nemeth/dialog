# dialog
With this JS class it's possible to create highly configurable dialog boxes with various options. This is an contao-component.

## Example usage

```
#!js

var dialog = new dnDialog({
	content: '<div class="mycontent">my content</div>'
});
```

## Available options

| Parameter           | Default   | Description                                                                     | Example                                                                                                             |
|---------------------|-----------|---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------|
| overlay             | true      | Show a overlay                                                                  |                                                                                                                     |
| overlayClose        | true      | Close the dialog with a click on overlay                                        |                                                                                                                     |
| overlayOpacity      | 0.8       | Opacity applied to overlay                                                      | 0.1 - 1                                                                                                             |
| id                  |           | Unique CSS ID for the dialog                                                    | dialog--payment                                                                                                     |
| className           |           | Additional class string applied to the top level dialog                         | dialog--payment-confirm                                                                                             |
| appendTo            | body      | Container the dialog will be append to                                          | .header                                                                                                             |
| size                | auto      | auto (Dialog size via CSS) or a percentage value (% of window width and height) | 95 (Always without '%' sign)                                                                                        |
| show                | true      | Show the dialog immediately. Usefull if the content will be applied later.      |                                                                                                                     |
| title               |           | Title for dialog (Applied to dialog header)                                     | Payment confirmation                                                                                                |
| content             |           | Content for dialog (Applied to dialog body)                                     | Do you really want to pay?                                                                                          |
| language            | en        | Override the default translation labels.                                        |                                                                                                                     |
| closeButton         | true      | Adds an close button to the dialog header.                                      |                                                                                                                     |
| closeLabel          | '&times;' | Label of the close button.                                                      | '&times;' or 'Close'                                                                                                |
| closeClass          |           | Applies the close event additionally to this CSS class.                         | close--dialog                                                                                                       |
| closeOnEsc          | true      | Close the dialog via ESC key.                                                   |                                                                                                                     |
| buttons/confirm     | true      | Default button: Confirm                                                         |                                                                                                                     |
| buttons/cancel      | true      | Default button: Cancel                                                          |                                                                                                                     |
| buttons/customs     | false     | Add additional buttons                                                          | customs: {    key1: {      label: 'button 1',      callback: function() { alert('test'); },      close: true    } } |
| closeOnConfirm      | true      | Close the dialog when confirm button is pressed.                                |                                                                                                                     |
| closeOnCancel       | true      | Close the dialog when cancel button is pressed.                                 |                                                                                                                     |
| callbacks/onOpen    |           | Fired when dialog is opened.                                                    | function() {this.test();}                                                                                           |
| callbacks/onLoaded  |           | Fired when dialog is loaded completly (After binding events).                   | function() {this.test();}                                                                                           |
| callbacks/onClose   |           | Fired when dialog is closed.                                                    | function() {this.test();}                                                                                           |
| callbacks/onConfirm |           | Fired when confirm button is pressed.                                           | function() {this.test();}                                                                                           |
| callbacks/onCancel  |           | Fired when cancel button is pressed.                                            | function() {this.test();}                                                                                           |
| callbacks/onRemove  |           | Fired when the dialog markup is removed.                                        | function() {this.test();}                                                                                           |


```
#!js

options:
{
	// Overlay
	overlay: true,				// Show overlay
	overlayClose: true,			// Close the overlay on click
	overlayOpacity: 0.2,	    // Overlay opacity

	// Dialog
	id:	'',				        // ID of dialog box
	className: '',				// Additional class string applied to the top level dialog
	appendTo: 'body',			// Container the dialog applies to
	size: 'auto',               // auto (CSS size from dialog-wrapper) or a percentage value (% of window width and height)

	// Behaviour
	show: true,				    // Show the dialog immediately by default

	// Content
	title: '',				    // The dialog title
	content: '',				// The dialog content
	language: 'en',             // Override default translation language

	// Close links
	closeButton: true,          // Whether include a close button
	closeLabel: '&times;',      // Label of the close button
	closeClass: '',				// Bind close event to this class
	closeOnEsc: true,           // Close the dialog via esc key

	// Buttons
	buttons: {
    	confirm:    true,       // Default button: confirm
    	cancel:     true,       // Default button: cancel
    	customs:    false       // Custom buttons
    }
	closeOnConfirm: true,		// Close the dialog on confirm button
	closeOnCancel: true,		// Close the dialog on cancel button

	// Callbacks
	/*,
	onOpen: $empty,             // Executes when dialog has opened
	onLoaded: $empty,           // Executes when dialog is loaded completly
	onClose: $empty,            // Executes when dialog is closed
	onRemove: $empty,           // Executes when dialog  markup is removed
	onConfirm: $empty,          // Executes when confirm button is clicked
	onCancel: $empty            // Executes when cancel button is clicked
	*/
}
```