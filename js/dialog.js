/**
 * Class dnDialog
 *
 * A highly configurable dialog box
 *
 * @copyright   Copyright (C) 2018 Daniel Nemeth
 * @author      Daniel Nemeth <office@daniel-nemeth.com>
 */
var dnDialog = new jqClass({

    /**
     * All available options
     */
    options: {
        overlay: true,              // Show overlay
        overlayClose: true,         // Close the dialog via a click on overlay
        overlayOpacity: 0.8,        // Overlay opacity
        id: '',                     // ID of dialog box
        className: null,            // Additional class string applied to the top level dialog
        closeButton: true,          // Whether include a close button
        closeLabel: '&times;',      // Label of the close button
        closeClass: '',             // Bind close event to this class
        closeOnConfirm: true,       // Close the dialog on confirm button
        closeOnCancel: true,        // Close the dialog on cancel button
        closeOnEsc: true,           // Close the dialog via esc key
        show: true,                 // Show the dialog immediately by default
        appendTo: 'body',           // Container to append dialog to
        title: '',                  // The dialog title
        content: '',                // The dialog content
        language: 'en',             // Override default translation language
        size: 'auto',               // auto (CSS size from dialog-wrapper) or a percentage value (% of window width and height)
        buttons: {                  // Buttons
            confirm:    true,       // Default button: confirm
            cancel:     true,       // Default button: cancel
            customs:    {           // Custom buttons
                key1: {
                    label:      '',
                    callback:   false,
                    close:      true
                }
            }
        },
        callbacks: {                // Callbacks
            onOpen:     false,
            onLoaded:   false,
            onClose:    false,
            onConfirm:  false,
            onCancel:   false,
            onRemove:   false
        }
    },

    /**
     * Languages
     */
    translations: {
        en: {
            confirm: 'Confirm',
            cancel: 'Cancel',
            close: 'Close'
        },
        de: {
            confirm: 'Bestätigen',
            cancel: 'Abbruch',
            close: 'Schliessen'
        }
    },

    /**
     * Templates for dialog box
     */
    templates: {
        overlay:
            '<div class="dndialog-overlay"></div>',
        dialog:
            '<div class="dndialog-wrapper">' +
                '<div class="dndialog-body">' +
                    '<div class="dndialog-content"></div>' +
                '</div>' +
            '</div>',
        header:
            '<div class="dndialog-header">' +
                '<div class="dndialog-title"></div>' +
            '</div>',
        footer:
            '<div class="dndialog-footer"></div>',
        closeButton:
            '<button type="button" class="dndialog-close-button">{closeButton}</button>',
        confirmButton:
            '<button type="button" class="dndialog-confirm-button">{confirmButton}</button>',
        cancelButton:
            '<button type="button" class="dndialog-cancel-button">{cancelButton}</button>'
    },

    /**
     * Dimensions
     */
    dimensions: {},

    /**
     * The jQuery dialog object
     */
    dialog: {},


    /**
     * Initalize the controller class
     */
    initialize: function(options)
    {
        this.setOptions(options);
        this.checkLanguage();

        // Set up the environment
        var dialog = $(this.templates.dialog);
        var dialogBody = dialog.find(".dndialog-body");
        var dialogContent = dialog.find('.dndialog-content');
        var buttons = this.createButtons();

        if (this.options.className)
            dialog.addClass(this.options.className);

        if (this.options.title) {
            dialogContent.before(this.templates.header);
            dialog.find(".dndialog-title").html(this.options.title);
        }

        if (this.options.closeButton) {
            dialog.find((this.options.title ? '.dndialog-header' : '.dndialog-body'))
            .append(this.templates.closeButton.replace('{closeButton}', this.options.closeLabel));
        }

        if (buttons.length) {
            dialogContent.after(this.templates.footer);
            dialog.find(".dndialog-footer").html(buttons);
        }

        // Add the dialog box to the container
        this.create(dialog);

        // Set dimensions
        this.setDimensions();

        // Open the box
        if (this.options.show)
            this.open();
    },


    /**
     * Set the default Language
     */
    checkLanguage: function()
    {
        // Check if language have been overriden with empty string
        if (this.options.language == '')
            this.options.language = 'en';
    },


    /**
     * Create the buttons
     * @return string
     */
    createButtons: function()
    {
        var str = '';

        if (this.options.buttons.confirm)
            str += this.templates.confirmButton.replace('{confirmButton}', this.translations[this.options.language]['confirm']);

        if (this.options.buttons.cancel)
            str += this.templates.cancelButton.replace('{cancelButton}', this.translations[this.options.language]['cancel']);

        if (this.options.buttons.customs) {
            var self = this;

            $.each(this.options.buttons.customs, function(key, button) {
                str += '<button type="button" class="dndialog-' + key + '-button">' + (button.label ? button.label : key) + '</button>';
                self.options.callbacks[key] = button.callback;
            });
        }

        return str;
    },


    /**
     * Create the overlay and dialog
     */
    create: function(dialog)
    {
        var self = this;

        if (this.options.overlay && !$('.dndialog-overlay').length) {
            var overlay = $(self.templates.overlay);

            overlay.css({
                display:    'none',
                opacity:    self.options.overlayOpacity,
                position:   'fixed',
                left:       0,
                top:        0,
            }).appendTo(self.options.appendTo);

            if (self.options.id)
                overlay.attr('id', 'dndialog-overlay-' + self.options.id);
        }

        this.dialog = $(dialog);

        if (self.options.id)
            this.dialog.attr('id', self.options.id);

        this.dialog.css('display', 'none').appendTo(self.options.appendTo);
    },


    /**
     * Set the dimensions to dimensions object
     */
    setDimensions: function()
    {
        this.dimensions = {
            w: typeof window.innerWidth === 'undefined' ? $(window).width() : window.innerWidth,
            h: typeof window.innerHeight === 'undefined' ? $(window).height() : window.innerHeight,
            d: [$(document).width(), $(document).height()],
            b: [this.dialog.width(), this.dialog.height()]
        }

        if (!isNaN(this.options.size) && this.options.size <= 100 && this.options.size > 0) {
            let pct = this.options.size + '%';

            this.dimensions.b = [pct, pct];
            this.dialog.width(pct);
            this.dialog.height(pct);
        } else {
            this.dimensions.b = [this.dialog.width(), this.dialog.height()];
        }

        this.updateDimensions();
    },


    /**
     * Update the dimensions and return as object
     */
    updateDimensions: function()
    {
        var self = this;

        // Overlay
        if (!$('.dndialog-overlay').length)
            return;

        $('.dndialog-overlay').css({
            width:      self.dimensions.d[0],
            height:     self.dimensions.d[1]
        });

        // Vertical position of dialog
        if (!isNaN(this.options.size) && this.options.size <= 100 && this.options.size > 0) {
            var mt = Math.round(((this.dimensions.h * (100 - this.options.size)) / 100) / 2);
            var ml = Math.round(((this.dimensions.w * (100 - this.options.size)) / 100) / 2);

            this.dialog.css({
                left: 0, 
                top: 0,
                marginTop: mt + 'px',
                marginLeft: ml + 'px'
            });
        } else {
            var mt = Math.round(this.dimensions.b[1] / 2);
            this.dialog.css('marginTop', '-' + mt + 'px');
        }
    },


    /**
     * Set content to the dialog
     */
    content: function(strContent)
    {
        if (!strContent)
            return '';

        this.options.content = strContent;
        this.dialog.find('.dndialog-content').html(strContent);
    },


    /**
     * Open the dialog box
     */
    open: function()
    {
        var self = this;

        // Execute the onOpen callback
        if ($.isFunction(self.options.callbacks.onOpen))
            this.options.callbacks.onOpen.call(this, this);

        // Display the overlay box
        if (this.options.overlay)
            $('.dndialog-overlay').show();

        // Display the dialog
        this.dialog.show(function() {
            // Execute the onLoaded callback
            if ($.isFunction(self.options.callbacks.onLoaded))
                self.options.callbacks.onLoaded.call(self, self);
        });

        // Bind events
        this.bindEvents();
    },


    /**
     * Close the dialog box
     */
    close: function()
    {
        var self = this;

        // Execute the onClose callback
        if ($.isFunction(this.options.callbacks.onClose))
            this.options.callbacks.onClose.call(this, this);

        // Check whether there is a dialog box or not
        if (!$('.dndialog-wrapper').length)
            return;

        // Don't close the overlay if there are dialog boxes left
        if ($('.dndialog-wrapper').length <= 1)
            $('.dndialog-overlay').hide();

        // Close the dialog box and delete it
        this.dialog.hide(function() {
            // Remove the events
            self.unbindEvents();

            // Remove from DOM
            $.when(self.dialog.remove()).then(function() {
                // Execute the onRemove callback
                if ($.isFunction(self.options.callbacks.onRemove))
                    self.options.callbacks.onRemove.call(self, self);
            });
        });
    },


    /**
     * Bind all events
     */
    bindEvents: function()
    {
        var self = this;

        // Update dimensions on resize
        $(window).on('resize.dndialog orientationchange.dndialog', function() {
            self.setDimensions()
        });

        // Bind the close event to any element with the options.closeClass
        if (this.options.closeClass != '') {
            this.dialog.find('.' + self.options.closeClass).on('click.dndialog', function(e) {
                e.preventDefault();
                self.close();
            });
        }

        // Close via click on overlay
        if (this.options.overlayClose) {
            $('.dndialog-overlay').on('click.dndialog', function(e) {
                e.preventDefault();
                self.close();
            });
        }

        // Close via escape key
        if (this.options.closeOnEsc) {
            $(document).on('keydown.dndialog', function(e) {
                // ESC key
                if (e.keyCode === 27) {
                    e.preventDefault();
                    self.close();
                }
            });
        }

        // Close button
        this.dialog.find('.dndialog-close-button').on('click.dndialog', function(e) {
            e.preventDefault();
            self.close();
        });

        // Confirm button
        if (this.options.buttons.confirm) {
            this.dialog.find('.dndialog-confirm-button').on('click.dndialog', function(e) {
                e.preventDefault();
                self.confirm();
            });
        }

        // Cancel button
        if (this.options.buttons.cancel) {
            this.dialog.find('.dndialog-cancel-button').on('click.dndialog', function(e) {
                e.preventDefault();
                self.cancel();
            });
        }

        // Custom callbacks
        if (this.options.buttons.customs) {
            $.each(this.options.buttons.customs, function(key, button) {
                self.dialog.find('.dndialog-' + key + '-button').on('click.dndialog', function(e) {
                    e.preventDefault();

                    // Execute the custom callback
                    if ($.isFunction(self.options.callbacks[key]))
                        self.options.callbacks[key].call(self, self);

                    // Close if defined
                    if (button.close)
                        self.close();
                });
            });
        }
    },


    /**
     * Unbind all events
     */
    unbindEvents: function()
    {
        var self = this;

        // Unbind options.closeClass
        if (this.options.closeClass)
            this.dialog.find('.' + this.options.closeClass).off('click.dndialog');

        // Unbind keydown for escape key
        $(document).off('keydown.dndialog');

        // Unbind window events like resize
        $(window).off('.dndialog');

        // Unbind the overlay click event
        $('.dndialog-overlay').off('click.dndialog');

        // Close button
        this.dialog.find('.dndialog-close-button').off('click.dndialog');

        // Confirm/Cancel button
        this.dialog.find('.dndialog-confirm-button').off('click.dndialog');
        this.dialog.find('.dndialog-cancel-button').off('click.dndialog');

        // Custom callbacks
        if (this.options.buttons.customs) {
            $.each(this.options.buttons.customs, function(key, button) {
                self.dialog.find('.dndialog-' + key + '-button').off('click.dndialog');
            });
        }
    },


    /**
     * Default confirm button action
     */
    confirm: function()
    {
        var self = this;

        // Execute the onConfirm callback
        if ($.isFunction(self.options.callbacks.onConfirm))
            this.options.callbacks.onConfirm.call(this, this);

        if (this.options.closeOnConfirm)
            this.close();
    },


    /**
     * Default cancel button action
     */
    cancel: function()
    {
        var self = this;

        // Execute the onConfirm callback
        if ($.isFunction(self.options.callbacks.onCancel))
            this.options.callbacks.onCancel.call(this, this);

        if (this.options.closeOnCancel)
            this.close();
    },


    /**
     * Update the content and don't close the dialog
     */
    updateContent: function(strContent)
    {
        if (!strContent)
            return;

        this.dialog.find('.dndialog-content').html(strContent);
        this.bindEvents();
    }
});